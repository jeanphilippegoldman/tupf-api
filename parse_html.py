# coding: utf-8
'''
nlp-analyse of html string
return lemma, positions in text and in html
'''

from html.parser import HTMLParser
import spacy


#https://docs.python.org/3/library/html.parser.html
class MyHTMLParser(HTMLParser):
    def handle_data(self, data):
        global texts, seendata, offsets, html_line_offsets
        line, pos = self.getpos()
        # print(line,pos)
        # print("#{}#{}#{}".format(data, seendata, html_line_offsets[line-1]+pos))
        texts.append(data)
        offsets.append((seendata, html_line_offsets[line-1]+pos))
        seendata += len(data)


    def handle_endtag(self, tag):
        global texts, seendata, offsets, html_line_offsets
        if tag in ['br', 'p', 'h1', 'h2', 'h3', 'h4']:
          texts.append(" ")
          seendata += 1



html =' <p dir="ltr" style="text-align: left;">Ceci est\n un <strong>test\n</strong>.</p>'
#' Ceci est\n un test\n.'
# 0 40 59 73
# str   html    txt
# ' '   0       0       line 1
# Ceci  40      1       line 1
# est   45      6       line 1
# ' '   49      10      line 2
# un    50      11      line 2
# test  61      14      line 2
# .     75      19      line 3

texts = []
offsets = []
seendata = 0
html_line_offsets = [0]  # char number of each beg of line


def parse_html(html, nlp):
    #what about \r\n and \r's ?
    global texts, offsets, seendata, html_line_offsets
    parser = MyHTMLParser()
    texts = []
    offsets = []
    seendata = 0
    lines = html.split('\n')
    html_line_lengths = [len(s) for s in lines]
    html_line_offsets = [0]     # char number of each beg of line
    for i in range(1, len(html_line_lengths)):
        html_line_offsets.append(html_line_offsets[i-1] + html_line_lengths[i-1] + 1)
    # print(html_line_lengths)
    # print(html_line_offsets)

    parser.feed(html)
    text = "".join(texts)
    # print("#"+text+"#")
    # print(offsets)  # correspondance between spans of html_text_data and position in raw text

    doc = nlp(text)
    response = [[w.text, w.lemma_, w.pos_, w.idx] for w in doc]
    # print("response")
    # print(response)
    for i_response in range(len(response)):
        idx = response[i_response][3]
        # print(response[i_response])
        j_offset = 0
        while j_offset < len(offsets) and response[i_response][3]>=offsets[j_offset][0]:
            # print("j={} {}>={}".format(j_offset, response[i_response][3], offsets[j_offset][0]))
            j_offset += 1
        j_offset -= 1
        # print(offsets[j_offset])
        response[i_response].append((offsets[j_offset][1] + idx - offsets[j_offset][0]))
    print(response)
    for i in range(len(response)):
      print(response[i], html[response[i][4]: response[i][4]+len(response[i][0])])
    return response

if __name__ == '__main__':
    print(parse_html(html))


