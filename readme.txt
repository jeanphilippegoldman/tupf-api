virtualenv --python=python3.8 env3
pip3 install flask spacy uwsgi
python3 -m spacy download fr_core_news_md
python3 -m spacy download en_core_web_md
python3 -m spacy download it_core_news_md
python3 -m spacy download es_core_news_md
python3 -m spacy download de_core_news_md


ssh -L 5000:localhost:5000 mia
python3 main.py

from postman:
GET localhost:5000/

POST localhost:5000/processtext
Body> raw > json : "source":"fr", "target": "en", "text": "Ceci est un test."}

launch with : uwsgi tupf.ini

curl --header "Content-Type: application/json"   --request POST   --data '{"source":"fr","target":"en", "text":"renard"}'   http://localhost:5060/processtext
curl --header "Content-Type: application/json"   --request POST   --data '{"source":"fr","target":"en", "text":"le <strong>renard</strong>\n dort."}'   http://localhost:5060/processhtml

https://miaparle.unige.ch/tupf/processtext?source=fr&target=en&text=le%20%20renard%20dort.
https://miaparle.unige.ch/tupf/processhtml?source=fr&target=en&text=le%20%3Cstrong%3E%20renard%20%3C/strong%3E%20dort.
https://miaparle.unige.ch/tupf/processhtml?source=fr&target=en&text=le%20%3Cb%3Erenard%3C/b%3E%20dort

 2047  2021-03-01 21:46:37 curl "http://localhost:5060/processtext?source=fr&target=en&text=le%20%20renard%20dort"

 2057  2021-03-02 17:50:45 curl "http://localhost:5000/processhtml?source=fr&target=en&text=<p>Bonjour,%20je%20suis%20<strong>Marc</strong>.</p>"
 2058  2021-03-02 17:51:10 curl "http://localhost:5000/processhtml?source=en&target=fr&text=<p>Hello,%20this%20is%20<strong>Marc</strong>.</p>"
 2015  2021-03-06 13:09:00 curl -X POST -H "Content-Type: application/json" -d @test1.json localhost:5060/processhtml
