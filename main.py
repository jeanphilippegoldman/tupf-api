# coding: utf-8
from flask import Flask, request
import json
import spacy
#from googletrans import Translator
import requests
import parse_html

app = Flask(__name__)
nlp={}
packages = {
    'en':'en_core_web_sm',
    'fr':'fr_core_news_sm',
    'it':'it_core_news_sm',
    'de':'de_core_news_sm',
    'es':'es_core_news_sm',
}

#for lang, package in langs.items():
#    print(f"Loading {package} for {lang}...")
#    nlp[lang] = spacy.load(package)

#translator = Translator()

text = """On ne connaît que les choses que l'on apprivoise, dit le renard. Les hommes n'ont plus le temps de rien connaître. Ils achètent des choses toutes faites chez les marchands. Mais comme il n'existe point de marchands d'amis, les hommes n'ont plus d'amis. Si tu veux un ami, apprivoise-moi!

Adieu, dit le renard. Voici mon secret. Il est très simple: on ne voit bien qu'avec le coeur. L'essentiel est invisible pour les yeux."""

l1 = "fr"
l2 = "en"

@app.route("/")
def hello():
    return "Hello World!"

def trans(text, sl, tl):

    # print("{} {} {}".format(text,l1,l2))
    # https://github.com/ssut/py-googletrans/issues/268

    # request_result = requests.get(f"https://translate.googleapis.com/translate_a/single?client=gtx&dt=t&sl={l1}&tl={l2}&q={text}")
    # translated_text = json.loads(request_result.text)[0][0][0]

    request_result = requests.get(f"https://clients5.google.com/translate_a/t?client=dict-chrome-ex&sl={sl}&tl={tl}&q={text}")
    translated_text = json.loads(request_result.text)['sentences'][0]['trans']
    return translated_text


@app.route('/processtext', methods=["GET", "POST"])
def processtext():
    if request.headers.get('Content-Type')=='application/json':
      args = request.get_json()
    else:
      args = request.args
    print(args)
    text = args["text"]
    source = args["source"]
    target = args["target"]

    print('#'+text+'#')
    #print(translator.translate("renard", src=l1, dest=l2).text)

    if source not in nlp.keys():
       print(f"Loading {packages[source]} for {source}...")
       nlp[source] = spacy.load(packages[source])
    doc = nlp[source](text)
    response = [[w.text, w.lemma_, w.pos_, w.idx, trans(w.lemma_, source, target)] for w in doc]
    #response = [ r.append(translator.translate(r[1], src=l1, dest=l2).text) for r in response]
    print(response)
    return json.dumps(response, ensure_ascii=True)


@app.route('/processhtml', methods=["GET", "POST"])
def processhtml():
    if request.method=="POST" and request.headers.get('Content-Type')=='application/json':
      args = request.get_json()
    else:
      args = request.args
    # print(args)
    html = args["text"]
    source = args["source"]
    target = args["target"]
    # print(translator.translate("renard", src=l1, dest=l2).text)

    # print(html)
    if source not in nlp.keys():
       print(f"Loading {packages[source]} for {source}...")
       nlp[source] = spacy.load(packages[source])
    response = parse_html.parse_html(html, nlp[source])
    # print(response)
    for r in response:
      r.append(trans(r[1], source, target))
    response = [r for r in response if r[2] not in ("PUNCT","SPACE")]
    # print(response)
    return json.dumps(response, ensure_ascii=True)


if __name__ == "__main__":
    app.run(host="0.0.0.0", port=5060, debug=True)
    # print(trans("renard", "fr", "en"))

